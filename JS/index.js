"use strict"
//1. Створення та використання рядків В JavaScript рядки можна створити, заключивши текст у лапки одного з трьох типів:
// подвійні лапки (“”), одинарні лапки (''), або зворотні лапки (``) template literals;
// або за дпомогою конструктора String;.
//2.Одинарні  та подвійні  лапки використовуються для простих рядків.
// Зворотні лапки використовуються для  створення багаторядкових рядків, та при використанні змінних.
//3.Рядки часто порівнюють за допомгою === (строге дорівнює) або за допомогою Метод localeCompare() та Метод Object.is()
//4.Метод Date.now() повертає кількість мілісекунд, які пройшли з 01.01.1970 до поточного часу.
//5.Date.now() повертає кількість мілісекунд, які пройшли з 01.01.1970 до поточного часу  а new Date() повертає об'єкт Date, який представляє поточну дату і час.

//1.
function isPalindrome(str) {
    const cleanedStr = str.replace(/[^a-zA-Z]/g, '').toLowerCase();

    for (let i = 0; i < Math.floor(cleanedStr.length / 2); i++) {
        if (cleanedStr[i] !== cleanedStr[cleanedStr.length - 1 - i]) {
            return false;
        }
    }

    return true;
}

const inputString = "Захист проходитиме в 5 аудиторії";
const result = isPalindrome(inputString);
console.log(result);

//2.
function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}
console.log(checkStringLength('checked string', 20));
console.log(checkStringLength('checked string', 10));

//3.
function calculateAge() {

    const birthDateString = prompt("Введіть вашу дату народження (у форматі РРРР-ММ-ДД):");

    const birthDate = new Date(birthDateString);

    const today = new Date();

    let age = today.getFullYear() - birthDate.getFullYear();

    const monthDifference = today.getMonth() - birthDate.getMonth();
    if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }

    return age;
}

const userAge = calculateAge();
console.log(`Вам ${userAge} років.`);



